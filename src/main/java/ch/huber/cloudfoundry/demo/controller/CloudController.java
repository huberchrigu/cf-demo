package ch.huber.cloudfoundry.demo.controller;

import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * @author christoph.huber
 */
@Profile("cloud")
@RestController
public class CloudController {
    private Cloud cloud;

    @PostConstruct
    public void initCloud() {
        this.cloud = new CloudFactory().getCloud();
    }

    @RequestMapping("/cloud")
    public String getCloudInfo() {
        ApplicationInstanceInfo info = cloud.getApplicationInstanceInfo();
        return "Application: " + info.getAppId() + "\n" +
                "Instance: " + info.getInstanceId() + "\n" +
                "Properties: \n" + info.getProperties();
    }

    @RequestMapping("/session/id")
    public Object getSessionId(HttpSession session) {
        Object sessionId = session.getAttribute("id");
        if (sessionId == null) {
            sessionId = UUID.randomUUID();
            session.setAttribute("id", sessionId);
        }
        return sessionId;
    }
}