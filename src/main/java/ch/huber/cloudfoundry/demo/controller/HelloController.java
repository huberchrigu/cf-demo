package ch.huber.cloudfoundry.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ConfigurableWebApplicationContext;

/**
 * @author christoph.huber
 */
@RestController
public class HelloController {
    private ConfigurableWebApplicationContext context;

    @Value("${demo.name:default}")
    private String name;

    @Autowired
    public HelloController(ConfigurableWebApplicationContext context) {
        this.context = context;
    }

    @RequestMapping("/hello")
    public String sayHello() {
        return "hello";
    }

    /**
     * Configuration
     */
    @RequestMapping("/name")
    public String tellYourName() {
        return name;
    }

    /**
     * Recovery
     */
    @RequestMapping("/bye")
    public void sayBye() {
        context.close();
    }
}