package ch.huber.cloudfoundry.demo.domain;

import org.springframework.data.repository.CrudRepository;

/**
 * @author christoph.huber
 */
public interface TaskRepository extends CrudRepository<Task, Long> {
}
